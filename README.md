# Berekenen van termijnen
Met behulp van deze API is het mogelijk een termijn bedrag te berekenen. Dit gebeurt dan op basis van twee input criteria. Daarna berekend de functie het bedrag dat per termijn betaald zou moeten worden. 

### Input
- termijnen (Integer)
- bedrag (double)

### Output
- Termijn bedrag


### Toekomstige functies
- termijn bedrag kan ook opgegeven worden (andere berekening)

## Hoe deze functie te gebruiken (ontwikkelen)
Voordat je het programma kunt gebruiken moet je eerst de volgende onderdelen geïnstalleerd hebben:
  - JDK versie 8 of hoger (https://aka.ms/azure-jdks)
  - Apache Maven, versie 3.0 of hoger (https://maven.apache.org/download.cgi)
  - Azure-CLI
  - .NET Core (https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial)
  - Node.js, versie 8.6 of hoger (https://nodejs.org/en/)

Om gebruik te kunnen maken van Azure Functions via de command line, is het belangrijk dat de .NET Core en Node.js zijn geïnstalleerd. Wanneer deze succesvol zijn geïnstalleerd, dan moet je het volgende command uitvoeren op de command line.

  `npm install -g azure-functions-core-tools@core`

Als het goed is wordt nu alles geïnstalleerd en de installatie kan je ook volgen op de command line.

Met het volgende command is het mogelijk om de functie locaal te gebruiken.
```
cd locatie-waar-het-project-staat
mvn clean package 
mvn azure-functions:run
```
Wanner dit gebeurt is, krijg je in je commandprompt normaal gezien een URL. Wanneer je deze URL kopiërt kan je nu de functie aanroepen. (vergeet niet de parameters toe te voegen wanneer die er zijn! e.g. ..url?bedrag=500&termijn=2)
