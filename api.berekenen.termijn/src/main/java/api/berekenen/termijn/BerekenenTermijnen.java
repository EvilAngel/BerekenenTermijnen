package api.berekenen.termijn;

import java.util.Optional;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.BindingName;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;

public class BerekenenTermijnen {

	private static final String REKENMETHODE_BEDRAG = "bedrag";
	private static final String REKENMETHODE_TERMIJN = "termijn";

	@FunctionName("berekenen-termijnen")
	public HttpResponseMessage run(@HttpTrigger(name = "req", methods = { HttpMethod.GET,
			HttpMethod.POST }, authLevel = AuthorizationLevel.FUNCTION) HttpRequestMessage<Optional<String>> request,
			final ExecutionContext context, @BindingName("rekenmethode") String rekenmethode,
			@BindingName("totaalBedrag") double totaalBedrag, @BindingName("termijn") int termijn,
			@BindingName("bedrag") double bedrag) {

		context.getLogger().info(String.format("Termijn berekenen met aantal termijnen = %s en termijnbedrag van : %s ",
				termijn, totaalBedrag));
		context.getLogger().info(
				String.format("De functie berekenen-termijnen wordt aangeroepen met de methode: %s ", rekenmethode));

		if (!isValid(totaalBedrag, termijn)) {
			return request.createResponseBuilder(HttpStatus.NOT_ACCEPTABLE)
					.body("Parameters zijn hebben geen geldige waarde").build();
		}

		if (REKENMETHODE_BEDRAG.equalsIgnoreCase(rekenmethode)) {
			berekenTermijnenOpBasisVanBedrag(totaalBedrag, bedrag);
			
		} else if (REKENMETHODE_TERMIJN.equalsIgnoreCase(rekenmethode)) {
			String bericht = berekenTermijnenOpBasisVanAantalTermijnen(totaalBedrag, termijn);
			return request.createResponseBuilder(HttpStatus.OK).body(bericht).build();
		}

		return request.createResponseBuilder(HttpStatus.NOT_ACCEPTABLE)
				.body("Parameter 'rekenmethode' is niet juist ingevuld. Waardes zijn: 'bedrag' of 'termijn'.").build();

	}

	private String berekenTermijnenOpBasisVanAantalTermijnen(double totaalBedrag, int termijn) {
		double bedragPerTermijn = totaalBedrag / termijn;

		return "Het bedrag per termijn is = €" + bedragPerTermijn;
	}

	private void berekenTermijnenOpBasisVanBedrag(double totaalBedrag, double bedrag) {
		// TODO Auto-generated method stub

	}

	private boolean isValid(double bedrag, int termijn) {
		return !(termijn == 0 || bedrag == 0);
	}
}
